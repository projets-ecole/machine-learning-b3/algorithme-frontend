# Import des dépendances
import pickle
from flask import Flask, render_template

# Instancier la variable de Flask
app = Flask(__name__)

# Les configs du site
app.config['DEBUG'] = True
app.config["ENV"] = "development"


@app.route('/')
def health_check():
    trained = pickle.load(open("A:/TOUT_LE_TRAVAIL/Cours/machineLearning/controle/algorithme-frontend/model.pkl", 'rb'))
    metrics = pickle.load(open("A:/TOUT_LE_TRAVAIL/Cours/machineLearning/controle/algorithme-frontend/metrics.pkl", 'rb'))
    loansNotPayInTime = pickle.load(open("A:/TOUT_LE_TRAVAIL/Cours/machineLearning/controle/algorithme-frontend/loansNotPayInTime.pkl", 'rb'))
    anomalies = pickle.load(open("A:/TOUT_LE_TRAVAIL/Cours/machineLearning/controle/algorithme-frontend/anomalies.pkl", 'rb'))


    return render_template("index.html", feature=trained.feature,importance=trained.importance,
                           importance_normalized=trained.importance_normalized, metrics=metrics,
                           loansNotPayInTime=loansNotPayInTime, anomalies=anomalies)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
